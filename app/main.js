"use strict";

(function() {
  var vertoHandle, currentCall, vertoCallbacks;

  /*
   * Initialize lib, cooking.
   * steps executed in init:
   * - test media permission
   * - check available devices
   */
  $.verto.init({}, bootstrap);

  /*
   * Callback for verto init.
   * @constructor
   * @param {boolean} status - has media permission
   */
  function bootstrap(status) {
    var wsURL = 'wss://' + window.location.hostname + ':8082';

    vertoHandle = new jQuery.verto({
      login: '1008@127.0.0.1',
      passwd: '1234',
      socketUrl: wsURL,
      tag: 'webcam',
      ringFile: 'sounds/bell_ring2.wav',
      deviceParams: {
        useCamera: true,
        useMic: true,
        useSpeak: true
      },
      iceServers: true
    }, vertoCallbacks);
    
    // do something in our app 
    document.getElementById("makeCall").addEventListener("click", makeCall);
    document.getElementById("hangupCall").addEventListener("click", hangupCall);
    // [...] 
  };

  function makeCall() {
    currentCall = vertoHandle.newCall({
      destination_number: "3520",
      caller_id_name: "Test",
      caller_id_number: "1008",
      outgoingBandwidth: "default",
      incomingBandwidth: "default",
      useVideo: true,
      useStereo: true,
      useCamera: true,
      useMic: true,
      useSpeak: true,
      dedEnc: false,
      mirrorInput: true,
      userVariables: {
        avatar: "",
        email: "test@test.com"
      }
    });
  };

  function hangupCall() {
    currentCall.hangup();
  };

  vertoCallbacks = {
    onMessage: onMessage,
    onDialogState: onDialogState,
    onWSLogin: onWSLogin,
    onWSClose: onWSClose,
    onEvent: onEvent
  };

  function onMessage(verto, dialog, message, data) {

  };

  function onDialogState(dialog) {
    console.debug('DialogState: ', dialog);
    
    if(!currentCall) {
      currentCall = dialog;
    };
  };

  function onWSLogin(verto, success) {

  };

  function onWSClose(verto, success) {

  };

  function onEvent(verto, event) {

  };

})();
